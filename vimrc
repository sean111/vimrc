
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'Lokaltog/vim-powerline'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'evidens/vim-twig'
Plugin 'ctrlp.vim'
Plugin 'mattn/emmet-vim'
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'mhumeSF/one-dark.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'scrooloose/syntastic'
Plugin 'bling/vim-airline'
Plugin 'xsbeats/vim-blade'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-easytags'
Plugin 'majutsushi/tagbar'
Plugin 'vitalk/vim-simple-todo'
Plugin 'wakatime/vim-wakatime'
Plugin 'tpope/vim-dispatch'
Plugin 'wincent/ferret'
Plugin 'tpope/vim-surround'
Plugin 'ervandew/supertab'
Plugin 'Shougo/neocomplete'
Plugin 'MattesGroeger/vim-bookmarks'
Plugin 'FelikZ/ctrlp-py-matcher'
call vundle#end()
filetype plugin indent on

set autoindent
set smartindent
syntax enable
set nostartofline
set ttyfast
set gdefault
set incsearch
set novb noeb
set bs=2

set encoding=utf-8
set cursorline
set t_Co=256
set background=dark
colorscheme onedark
set numberwidth=3
set number

set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab

set omnifunc=syntaxcomplete#Complete

let g:ctrlp_custom_ignore = 'bower_components\|node_modules\|DS_Store\|git'
let g:ctrlp_match_window_bottom = 0 " Show at top of window
let g:ctrlp_working_path_mode = 2 " Smart path mode
let g:ctrlp_mru_files = 1 " Enable Most Recently Used files feature
let g:ctrlp_jump_to_buffer = 2 " Jump to tab AND buffer if already open
"let g:ctrlp_split_window = 1 " <CR> = New Tab
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_quit_key='<C-c>'
let g:mustache_abbreviations = 1

"Nerdtree
"autocmd vimenter * if !argc() | NERDTree | endif " Load NERDTree by default for directory
map <Leader>nt :NERDTreeToggle<CR>
let NERDTreeShowHidden=1
"map <Leader>t <plug>NERDTreeTabsToggle<CR>

let g:Powerline_symbols = 'fancy'

"CTRL-P
if executable('ag')
  set grepprg=ag\ --nogroup\ --nocolor
  "let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
  let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
              \ --ignore .git
              \ -- ignore .svn
              \ --ignore .hg
              \ --ignore .DS_Store
              \ -g ""'
  let g:ctrlp_use_caching = 0
endif
let g:ctrlp_switch_buffer = 'Et'
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }

augroup reload_vimrc " {
	autocmd!
	autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }
set modelines=1

"syntastic
nmap <leader>st :SyntasticToggleMode<cr>
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"Syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
"let g:syntastic_php_checkers = ['php', 'phpcs', 'phpmd']
let g:syntastic_php_checkers = ['php', 'phpmd']
"let g:syntastic_auto_loc_list = 1
let g:syntastic_loc_list_height = 5
let g:syntastic_javascript_checkers = ['eslint']

"vim-airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
"let g:airline_theme = 'powerlineish'

"gitgutter
let g:gitgutter_enabled = 0
"let g:gitgutter_highlight_lines = 1
nmap <leader>gt :GitGutterToggle<cr>

set laststatus=2


"Misc Mappings
nnoremap <F5> :buffers<CR>:buffer<Space>
nnoremap <Leader>bl :buffers<CR>:buffer<Space>
nnoremap <F6> :bprevious<CR>
nnoremap <Leader>bb :bprevious<CR>
nnoremap <F7> :bnext<CR>
nnoremap <Leader>bn :bnext<CR>
nnoremap <Leader>bc :bd<CR>
nnoremap <Leader>tb :TagbarToggle<CR>
nnoremap <Leader>tu :UpdateTags!<CR>
nnoremap Y y$
nnoremap D d$
nnoremap <C-Left> :vertical resize +1<CR>
nnoremap <C-Right> :vertical resize -1<CR>
nnoremap <C-Up> :resize +1<CR>
nnoremap <C-Down> :resize -1<CR>
nmap <M-Up> [e
nmap <M-Down> ]e
vmap <M-Up> [egv
vmap <M-Down> ]egv

let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_quit_key='<C-c>'

"neocomplete
let g:neocomplete#enable_at_startup = 1
